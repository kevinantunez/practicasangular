import { Component, OnInit,Input, HostBinding } from '@angular/core';
import {Tarea} from '../models/tarea.model'

@Component({
  selector: 'app-detalle-tarea',
  templateUrl: './detalle-tarea.component.html',
  styleUrls: ['./detalle-tarea.component.css']
})
export class DetalleTareaComponent implements OnInit {
  @Input() tarea:Tarea;
  @HostBinding('class') decorador;
  constructor() { }

  ngOnInit() {
    switch(this.tarea.prioridad){
      case 1:
       this.decorador= "list-group-item list-group-item-danger"
       break;
      case 2:
       this.decorador= "list-group-item list-group-item-primary"
       break;
      case 3:
       this.decorador= "list-group-item list-group-item-success"
       break;
      case 4:
       this.decorador= "list-group-item list-group-item-warning"
       break;
    }
  }

}
