export class Tarea{
    titulo:string;
    descripcion:string;
    prioridad:number;

    constructor(t:string,d:string,p:number){
        this.titulo = t;
        this.descripcion=d;
        this.prioridad=p;
    }
}