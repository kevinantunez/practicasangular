import { Component, OnInit, HostBinding } from '@angular/core';
import {Tarea} from '../models/tarea.model'
@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {
  listaTareas:Tarea[];
  constructor() {
    this.listaTareas = [];
    this.listaTareas.push(new Tarea("Aprender Angular 6","Ver los videos del Curso Desarrollo de Paginas Web con Angular :)",1))
    this.listaTareas.push(new Tarea("Descargar Node.js","Ir a la siguiente Pagina https://nodejs.org/es/ y Descargar node.js",2))
    this.listaTareas.push(new Tarea("Instalar Angular","Para instalar angular se necesita ejecutar npm install -g @angular/cli en la terminal",3))
    this.listaTareas.push(new Tarea("Crear Primer Proyecto Angular","Ejecutar en la terminal ng new nombre-del-proyecto",4))

   }

  ngOnInit() {
  }
  guardar(t:string,d:string,p:string):boolean{
   this.listaTareas.push(new Tarea(t,d,parseInt(p)))
   this.listaTareas.sort((a,b)=>a.prioridad-b.prioridad);
   console.log(this.listaTareas)
   return false
  }
}
